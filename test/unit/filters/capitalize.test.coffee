'use strict'

describe 'Capitalize Filter', ->

  beforeEach module 'debtTracker'

  capitalize = undefined

  beforeEach inject (capitalizeFilter) ->
    capitalize = capitalizeFilter

  it 'should capitalize the first letter of a word', ->
    expect capitalize('stuff')
      .toEqual 'Stuff'