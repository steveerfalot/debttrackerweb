'use strict'

describe 'DebtorController', ->

  beforeEach module('debtTracker')

  http = undefined
  httpService = undefined
  scope = undefined
  rootScope = undefined
  state = undefined
  stateParams = undefined

  testUrl = 'http://localhost:8080/DebtTrackerWeb/'

  beforeEach inject (_$httpBackend_, _httpService_, $rootScope, $controller, $state, $stateParams) ->
    http = _$httpBackend_
    httpService = _httpService_
    rootScope = $rootScope
    state = $state
    stateParams = $stateParams

    rootScope.entity = {
      type: ''
      new: {}
      index: ->
      show: (id) ->
      create: (debtor) ->
      del: (debtor) ->
    }
    scope = rootScope.$new()

    $controller 'DebtorController', $scope:scope

    http.whenGET 'views/main.html'
    .respond 'who cares'

    http.whenGET 'views/common/index.html'
    .respond 'who cares'

  describe 'index', ->

    it 'should call entity.index', ->
      spyOn scope.entity, 'index'

      scope.index()

      expect scope.entity.index
        .toHaveBeenCalled()

  describe 'show', ->

    it 'should call entity.show with an id', ->
      spyOn scope.entity, 'show'

      scope.show 1

      expect scope.entity.show
        .toHaveBeenCalledWith 1

  describe 'create', ->

     it 'should call entity.create with an entity', ->
       spyOn scope.entity, 'create'

       scope.create {name: 'bear'}

       expect scope.entity.create
        .toHaveBeenCalledWith {name: 'bear'}

  describe 'del', ->

    it 'should call entity.del with a debtee', ->
      spyOn scope.entity, 'del'

      scope.del {name: 'delete bears'}

      expect scope.entity.del
        .toHaveBeenCalledWith {name: 'delete bears'}

  describe 'on build', ->

    it 'should call entity.index if state.current.name is home.debtor', inject ($controller) ->
      spyOn scope.entity, 'index'

      state.current.name = 'home.debtor'
      $controller 'DebtorController', $scope:scope

      expect scope.entity.index
      .toHaveBeenCalled()

    it 'should call entity.show if state.current.name is home.debtorShow', inject ($controller) ->
      spyOn scope.entity, 'show'

      state.current.name = 'home.debtorShow'
      $controller 'DebtorController', $scope:scope

      expect scope.entity.show
      .toHaveBeenCalledWith stateParams.id