'use strict'

describe 'MainController', ->

  beforeEach module('debtTracker')

  scope = undefined
  rootScope = undefined
  state = undefined
  httpService = undefined
  cleanUpService = undefined
  http = undefined
  mockEntity = undefined

  beforeEach inject ($rootScope, $state, $controller, _httpService_, _$httpBackend_, _cleanUpService_) ->
    http = _$httpBackend_
    httpService = _httpService_
    cleanUpService = _cleanUpService_
    state = $state

    scope = $rootScope.$new()
    mockEntity = {type: 'debtee', id: 1}
    scope.entity =
        type = 'debtee'

    http.whenGET 'views/main.html'
    .respond 'who cares'

    http.whenGET 'views/common/index.html'
    .respond 'who cares'

    $controller 'MainController', $scope: scope

  it 'should define entity.type', ->
    expect scope.entity.type
      .toEqual ''

  it 'should define new', ->
    expect scope.entity.new
      .toEqual {}

  describe 'entity.index', ->

    it 'should call httpService.get', ->
      spyOn httpService, 'get'

      scope.entity.index()

      expect httpService.get
        .toHaveBeenCalledWith scope

    it 'should assign a list of entities to entity[entityType]', ->
      scope.entity.type = 'debtee'
      http.whenGET App.urlToServer + 'debtee'
      .respond [{id:1}]
      spyOn httpService, 'get'
      .and.callThrough()

      scope.entity.index()
      http.flush()

      expect scope.entity[scope.entity.type]
        .toEqual [{id:1}]

  describe 'entity.show', ->

    it 'should call httpService.getOne', ->
      spyOn httpService, 'getOne'

      scope.entity.show 1

      expect httpService.getOne
        .toHaveBeenCalledWith scope, 1

  describe 'create', ->

    it 'should call httpService.post', ->
      spyOn httpService, 'post'

      scope.entity.create {name: 'Bob Robertson'}

      expect httpService.post
        .toHaveBeenCalledWith scope, {name: 'Bob Robertson'}

  describe 'del', ->

    it 'should call httpService.del with an object containing a type and ids', ->
      spyOn state, 'go'
      spyOn httpService, 'del'

      scope.entity.del mockEntity

      expect httpService.del
        .toHaveBeenCalledWith scope

    it 'should call state.go', ->
      spyOn state, 'go'

      scope.entity.del mockEntity

      expect state.go
        .toHaveBeenCalledWith 'home.' + scope.entity.type

  describe 'entity.goToShow', ->

    it 'should call $broadcast', ->
      spyOn state, 'go'
      spyOn scope, '$broadcast'

      scope.entity.goToShow 1

      expect scope.$broadcast
        .toHaveBeenCalledWith 'goToShow' + scope.entity.type

    it 'should call state.go', ->
      spyOn state, 'go'

      scope.entity.goToShow 1

      expect state.go
        .toHaveBeenCalledWith 'home.' + scope.entity.type + 'Show', {id: 1}

  describe 'goToList', ->

    it 'should call $broadcast with [entity]List as a parameter', ->
      spyOn state, 'go'
      spyOn scope, '$broadcast'

      scope.goToList('debtee')

      expect scope.$broadcast
        .toHaveBeenCalledWith 'debteeList'

    it 'should call cleanUpService.cleanScopeEntityNew', ->
      spyOn state, 'go'
      spyOn cleanUpService, 'cleanScopeEntityNew'

      scope.goToList('debt')

      expect cleanUpService.cleanScopeEntityNew
        .toHaveBeenCalledWith scope

    it 'should call state.go', ->
      spyOn state, 'go'

      scope.goToList('debtor')

      expect state.go
        .toHaveBeenCalledWith 'home.debtor'

  return