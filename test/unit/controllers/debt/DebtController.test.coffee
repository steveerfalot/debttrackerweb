'use strict'

describe 'DebtController', ->

  beforeEach module('debtTracker')

  scope = undefined
  rootScope = undefined
  state = undefined
  stateParams = undefined
  http = undefined

  beforeEach inject ($controller, $rootScope, $state, $stateParams, $httpBackend) ->
    state = $state
    http = $httpBackend
    stateParams = $stateParams

    $rootScope.entity =
      type: ''
      new: {}
      index: ->
      show: (id) ->
      create: (debt) ->
      del: (debt) ->

    scope = $rootScope.$new()

    http.whenGET 'views/main.html'
      .respond 'who cares'

    $controller 'DebtController', $scope: scope


  describe 'init', ->

    it 'should set entity.type to debt', ->
      expect scope.entity.type
        .toEqual 'debt'

    it 'should set entity.new.debt to empty object', ->
      expect scope.entity.new.debt
        .toEqual {}

    it 'should call entity.index when state.current.name is home.debt', inject ($controller) ->
      spyOn scope.entity, 'index'

      state.current.name = 'home.debt'
      scope.init()

      expect scope.entity.index
      .toHaveBeenCalled()

    it 'should call entity.show when state.current.name is home.debtShow', inject ($controller) ->
      #arrange
      spyOn scope.entity, 'show'
      #act
      state.current.name = 'home.debtShow'
      scope.init()
      #assert
      expect scope.entity.show
      .toHaveBeenCalledWith stateParams.id

    it 'should call new when state.current.name is home.debtNew', inject ($controller) ->
      spyOn scope, 'new'
      state.current.name = 'home.debtNew'

      scope.init()

      expect scope.new
        .toHaveBeenCalled()

  describe 'index', ->

    it 'should call entity.index', ->
      spyOn scope.entity, 'index'

      scope.index()

      expect scope.entity.index
      .toHaveBeenCalled()

  describe 'show', ->

    it 'should call entity.show', ->
      spyOn scope.entity, 'show'

      scope.show 1

      expect scope.entity.show
      .toHaveBeenCalledWith 1

  describe 'create', ->

    it 'should call entity.create', ->
      spyOn scope.entity, 'create'

      scope.create {name: 'Bob Robertson'}

      expect scope.entity.create
      .toHaveBeenCalledWith {name: 'Bob Robertson'}

  describe 'del', ->

    it 'should call entity.del with a debtee', ->
      spyOn scope.entity, 'del'
      mockEntity = {id: 1, name: 'Dumb Ass'}

      scope.del mockEntity

      expect scope.entity.del
      .toHaveBeenCalledWith mockEntity

  describe 'new', ->

    it 'should call http.get for debtees', ->
      http.whenGET App.urlToServer + 'debtor'
        .respond []
      http.expectGET App.urlToServer + 'debtee'
        .respond []
      scope.new()
      http.flush()

    it 'should set debtees to the response of get for debtees', ->
      http.whenGET App.urlToServer + 'debtor'
        .respond []
      response = [{name: 'debtee'}]
      http.expectGET App.urlToServer + 'debtee'
        .respond response
      scope.new()
      http.flush()
      expect scope.debtees
        .toEqual response

    it 'should call http.get for debtors', ->
      http.whenGET App.urlToServer + 'debtee'
        .respond []
      http.expectGET App.urlToServer + 'debtor'
        .respond []
      scope.new()
      http.flush()

    it 'should set debtors to the response of get for debtors', ->
      http.whenGET App.urlToServer + 'debtee'
        .respond []
      response = [{name: 'debtor'}]
      http.expectGET App.urlToServer + 'debtor'
        .respond response
      scope.new()
      http.flush()
      expect scope.debtors
        .toEqual response

