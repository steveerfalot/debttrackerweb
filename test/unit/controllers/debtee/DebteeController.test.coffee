'use strict'

describe 'DebteeController', ->

  beforeEach module('debtTracker')

  http = undefined
  httpService = undefined
  scope = undefined
  rootScope = undefined
  state = undefined
  stateParams = undefined

  testUrl = 'http://localhost:8080/DebtTrackerWeb/'

  beforeEach inject (_$httpBackend_, _httpService_, $rootScope, $controller, $state, $stateParams) ->
    http = _$httpBackend_
    httpService = _httpService_
    rootScope = $rootScope
    state = $state
    stateParams = $stateParams
    rootScope.entity = {
      type: ''
      new: {}
      index: ->
      show: (id) ->
      create: (entity) ->
      del: (debtee) ->
    }
    scope = rootScope.$new()

    $controller 'DebteeController', $scope:scope

    http.whenGET 'views/main.html'
    .respond 'who cares'

    http.whenGET 'views/debtee/index.html'
    .respond 'who cares'

  describe 'on build', ->

    it 'should define entity.type as debtee', ->
      expect scope.entity.type
        .toEqual 'debtee'

    it 'should define entity.new.debtee as an empty object', ->
      expect scope.entity.new.debtee
        .toEqual {}

    it 'should call entity.index when state.current.name is home.debtee', inject ($controller) ->
      spyOn scope.entity, 'index'

      state.current.name = 'home.debtee'
      $controller 'DebteeController', $scope:scope

      expect scope.entity.index
        .toHaveBeenCalled()

    it 'should call entity.show when state.current.name is home.debteeShow', inject ($controller) ->
      #arrange
      spyOn scope.entity, 'show'
      #act
      state.current.name = 'home.debteeShow'
      $controller 'DebteeController', $scope:scope
      #assert
      expect scope.entity.show
      .toHaveBeenCalledWith stateParams.id

  describe 'index', ->

    it 'should call entity.index', ->
      spyOn scope.entity, 'index'

      scope.index()

      expect scope.entity.index
      .toHaveBeenCalled()

  describe 'show', ->

    it 'should call entity.show', ->
      spyOn scope.entity, 'show'

      scope.show 1

      expect scope.entity.show
        .toHaveBeenCalledWith 1

  describe 'create', ->

    it 'should call entity.create', ->
      spyOn scope.entity, 'create'

      scope.create {name: 'Bob Robertson'}

      expect scope.entity.create
        .toHaveBeenCalledWith {name: 'Bob Robertson'}

  describe 'del', ->

    it 'should call entity.del with a debtee', ->
      spyOn scope.entity, 'del'
      mockEntity = {id: 1, name: 'Dumb Ass'}

      scope.del mockEntity

      expect scope.entity.del
        .toHaveBeenCalledWith mockEntity