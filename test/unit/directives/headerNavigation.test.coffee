'use strict'

describe 'headerNavigation directive', () ->

  beforeEach module 'debtTracker'
  beforeEach module 'views'

  scope = undefined
  element = undefined
  compile = undefined
  amountOfLinks = 3

  beforeEach inject ($rootScope, $compile) ->
    scope = $rootScope.$new()
    scope.goToList = {}
    compile = $compile
    element = angular.element '<header-navigation></header-navigation>'
    compile(element)(scope)
    scope.$digest()

  it 'should be able to be used as an attribute', ->
    attributeElement = angular.element '<div header-navigation></div>'
    compile(attributeElement)(scope)
    scope.$digest()
    expect attributeElement.find('li').length
      .toEqual amountOfLinks

  it 'should replace header-navigation element with navigation list', () ->
    expect(element.find('li').length)
        .toEqual amountOfLinks

  it 'should add Debtee pill to navigation.headerPills', () ->
    expect scope.navigation
        .toContain
        headerPills: [
          {
            name: 'Debtee',
            goTo: () -> scope.goToDebtees()
          }
        ]

  it 'should add Debtor pill to navigation.headerPills', () ->
    expect scope.navigation
        .toContain
        headerPills: [
          {
            name: 'Debtor',
            goTo: () -> scope.goToDebtors()
          }
        ]

  it 'should add Debt pill to navigation.headerPills', () ->
    expect scope.navigation
        .toContain
        headerPills: [
          {
            name: 'Debt',
            goTo: () -> scope.goToDebts()
          }
        ]

  it 'should call goToList when navigation.goTo is called', () ->
    spyOn scope, 'goToList'
    scope.navigation.goTo('debtee')
    expect scope.goToList
        .toHaveBeenCalledWith 'debtee'
