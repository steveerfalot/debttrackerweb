'use strict'

describe 'manyToManyListLoader directive', () ->

  beforeEach module 'debtTracker'
  beforeEach module 'views'

  scope = undefined
  element = undefined
  compile = undefined
  httpBackend = undefined
  returnData = undefined

  beforeEach inject ($rootScope, $compile, $httpBackend) ->
    scope = $rootScope.$new()
    scope.debt = {
      name: 'dude'
    }
    compile = $compile
    httpBackend = $httpBackend
    element = angular.element '<many-to-many-list-loader this-id="1" this-type="debt" many-type="debtee" this-debt="debt"></many-to-many-list-loader>'
    compile(element)(scope)
    returnData = {
      data: [
        {name: 'dude'},
        {name: 'guy'}
      ]
    }
    httpBackend.expectGET App.urlToServer + 'debt/1/debtee'
    .respond returnData

  it 'should call http.get with entity, type, and id', ->
    scope.$digest()
    httpBackend.flush()

  it 'should add debtee list to debt.debtee', ->
    scope.$digest()
    httpBackend.flush()
    expect scope.debt.debtee
      .toEqual returnData



