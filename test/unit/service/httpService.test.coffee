'use strict'

describe 'httpService', () ->

  beforeEach module 'debtTracker'

  http = undefined
  httpService = undefined
  scope = undefined
  state = undefined

  beforeEach inject (_$httpBackend_, _httpService_, $rootScope, $state) ->
    http = _$httpBackend_
    httpService = _httpService_
    state = $state

    $rootScope.entity = {
      type: 'debtee'
    }

    scope = $rootScope.$new()

    http.whenGET 'views/main.html'
      .respond 'who cares'
    http.whenGET 'views/common/personIndex.html'
      .respond 'who cares'

  it 'should have a function called get', () ->
    expect typeof httpService.get
      .toEqual 'function'

  describe 'get', () ->

    it 'should call http.get with url to server and entity to get', () ->
      #arrange

      http.expectGET App.urlToServer + 'debtee'
        .respond [{id: 1}]
      #act
      httpService.get scope
      http.flush()
      #assert

    it 'should return a list of entities', () ->
      http.expectGET App.urlToServer + 'debtee'
        .respond [{id:1},{id:2}]

      httpService.get scope
      http.flush()

      expect scope.entity[scope.entity.type]
        .toEqual [{id:1},{id:2}]

    it 'should return an error if the response is anything but 200', () ->
      spyOn console, 'log'
      http.expectGET App.urlToServer + scope.entity.type
        .respond 500

      httpService.get scope
      http.flush()

      expect console.log
        .toHaveBeenCalledWith 'Something went wrong in httpService.get for: ', 'debtee'

  describe 'getOne', () ->

      it 'should call http.get with url to server and entity to get', () ->
        #arrange
        http.expectGET App.urlToServer + 'debtee/1'
          .respond [{id: 1}]
        #act
        httpService.getOne scope, '1'
        http.flush()
        #assert

      it 'should return one entity', () ->
        http.expectGET App.urlToServer + 'debtee/1'
          .respond {id:1, name: 'Bob'}

        httpService.getOne scope, '1'
        http.flush()

        expect scope.entity[scope.entity.type]
          .toEqual {id: 1, name: 'Bob'}

      it 'should return an error if the response is anything but 200', () ->
        spyOn console, 'log'
        http.expectGET App.urlToServer + 'debtee/1'
          .respond 500

        httpService.getOne scope, '1'
        http.flush()

        expect console.log
          .toHaveBeenCalledWith 'Something went wrong in httpService.getOne for: ', 'debtee'

  describe 'post', () ->
      it 'should call http.post with url to server and body that contains entity to save', () ->
        #arrange
        http.expectPOST App.urlToServer + 'debtee', {name: 'Dude Duder'}
        #act
        httpService.post scope, 'debtee', {name: 'Dude Duder'}
        #assert

      it 'should return the saved entity', () ->
        #arrange
        http.expectPOST App.urlToServer + 'debtee', {name: 'Dude Duder'}
          .respond {name: 'Dude Duder'}
        #act
        httpService.post scope, {name: 'Dude Duder'}
        http.flush()
        #assert
        expect scope.savedEntity['debtee']
          .toEqual {name: 'Dude Duder'}

      it 'should return an error for any response other than 200', () ->
        #arrange
        spyOn console, 'log'
        http.expectPOST App.urlToServer + 'debtee', {name: 'Dude Duder'}
          .respond 500
        #act
        httpService.post scope, {name: 'Dude Duder'}
        http.flush()
        #assert
        expect console.log
          .toHaveBeenCalledWith 'Something went wrong in httpService.post for: ', 'debtee'

  describe 'del', () ->

      it 'should call http.del', ->
        scope.entity.debtee = {
          id: 1
        }
        http.expectDELETE App.urlToServer + 'debtee/1'
          .respond '1'
        httpService.del scope
        http.flush()

      it 'should call state.go with appropriate parameters', ->
        scope.entity.debtee = {
          id: 1
        }
        http.expectDELETE App.urlToServer + 'debtee/1'
          .respond '1'
        spyOn state, 'go'

        httpService.del scope
        http.flush()

        expect state.go
          .toHaveBeenCalledWith 'home.debtee', {}, {reload: true}

      it 'should return an error on response other than 200', ->
        scope.entity.debtee = {
          id: 1
        }
        spyOn console, 'log'
        http.expectDELETE App.urlToServer + 'debtee/1'
          .respond 500
        httpService.del scope
        http.flush()
        expect console.log
          .toHaveBeenCalledWith 'Something went wrong in httpService.del deleting: ', 'debtee'
