'use strict'

describe 'cleanUpService', () ->

  beforeEach module 'debtTracker'

  cleanUpService = undefined
  scope = undefined

  beforeEach inject (_cleanUpService_, $rootScope) ->
    cleanUpService = _cleanUpService_

    $rootScope.entity = {
      type: 'debtee'
      new: {
        debtee: 'Dude'
      }
    }

    scope = $rootScope.$new()

  describe 'cleanScopeEntityNew', ->

    it 'should empty scope.entity.new', ->
      expect scope.entity.new
        .toEqual {debtee: 'Dude'}

      cleanUpService.cleanScopeEntityNew(scope)

      expect scope.entity.new
        .toEqual {}

    it 'should do nothing if scope.entity is undefined', ->
      scope.entity = undefined

      cleanUpService.cleanScopeEntityNew(scope)

      expect scope.entity
        .toBeUndefined()
