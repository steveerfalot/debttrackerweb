'use strict'

debtTracker.directive 'headerNavigation', () ->
  restrict: 'AE',
  replace: true,
  templateUrl: App.templateUrl + 'navigation/headerNavigation.html',
  link: (scope, element, attributes) ->
    scope.navigation =
      headerPills: [
        {name: 'debtee'},
        {name: 'debtor'},
        {name: 'debt'}
      ],
      goTo: (type) ->
        scope.goToList(type)
