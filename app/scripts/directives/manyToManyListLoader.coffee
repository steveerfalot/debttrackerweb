'use strict'

debtTracker.directive 'manyToManyListLoader', ($http) ->
  restrict: 'AE',
  replace: true,
  template: '<div ng-repeat="debtee in debt.debtees">{{debtee.name}}</div>',
  scope: {
    manyType: '=manyType',
    thisType: '=thisType',
    thisId: '=thisId',
    thisDebt: '=thisDebt'
  },
  link: (scope, element, attributes) ->
    scope[attributes.thisType] = {}
    $http.get App.urlToServer + attributes.thisType + '/' + attributes.thisId + '/' + attributes.manyType
      .success (data) ->
        console.log 'attributes.manyType: ', attributes.manyType
        scope[attributes.thisType][attributes.manyType] = data
