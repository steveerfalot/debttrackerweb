'use strict'

angular.module('debtTracker')
  .controller 'MainController', ($scope, $state, $rootScope, httpService, cleanUpService) ->

    $scope.entity = {
      type: ''
      new: {}
    }

    $scope.entity.index = ->
      httpService.get $scope

    $scope.entity.show = (id) ->
      httpService.getOne $scope, id

    $scope.entity.create = (payload) ->
      httpService.post $scope, payload

    $scope.entity.del = (entity) ->
      $scope.entity[$scope.entity.type] = entity
      httpService.del $scope
      $state.go 'home.' + $scope.entity.type

    $scope.entity.goToShow = (id, type) ->
      entityType = type || $scope.entity.type
      $scope.$broadcast 'goToShow' + $scope.entity.type
      $state.go 'home.' + entityType + 'Show', {id: id}

    $scope.goToList = (type) ->
      $scope.$broadcast type + 'List'
      cleanUpService.cleanScopeEntityNew($scope)
      $state.go 'home.' + type