'use strict'

debtTracker
.controller 'DebtController', ($scope, $state, $stateParams, $http) ->

  $scope.entity.type = 'debt'
  $scope.entity.new[$scope.entity.type] = {}

  $scope.index = ->
    $scope.entity.index()

  $scope.show = (id) ->
    $scope.entity.show id

  $scope.new = ->
    $http.get App.urlToServer + 'debtee'
      .success (data) ->
        $scope.debtees = data
    $http.get App.urlToServer + 'debtor'
      .success (data) ->
        $scope.debtors = data

  $scope.create = (payload) ->
    $scope.entity.create payload

  $scope.del = (debt) ->
    $scope.entity.del debt

  $scope.init = ->
    if $state.current.name == 'home.debt' then $scope.index()
    if $state.current.name == 'home.debtNew' then $scope.new()
    if $state.current.name == 'home.debtShow' then $scope.show($stateParams.id)

  $scope.init()

  return