'use strict'

debtTracker
.controller 'DebtorController', ($scope, httpService, $state, $stateParams) ->

  $scope.entity.type = 'debtor'
  $scope.entity.new[$scope.entity.type] = {}

  $scope.index = ->
    $scope.entity.index()

  $scope.show = (id) ->
    $scope.entity.show(id)

  $scope.create = (payload) ->
    $scope.entity.create(payload)

  $scope.del = (debtor) ->
    $scope.entity.del debtor

  if $state.current.name == 'home.debtor' then $scope.index()
  if $state.current.name == 'home.debtorShow' then $scope.show($stateParams.id)