'use strict'

angular.module('debtTracker')
.controller 'DebteeController', ($scope, $state, $stateParams) ->

  $scope.entity.type = 'debtee'
  $scope.entity.new[$scope.entity.type] = {}

  $scope.index = ->
    $scope.entity.index()

  $scope.show = (id) ->
    $scope.entity.show id

  $scope.create = (payload) ->
    $scope.entity.create payload

  $scope.del = (debtee) ->
    $scope.entity.del debtee

  if $state.current.name == 'home.debtee' then $scope.index()
  if $state.current.name == 'home.debteeShow' then $scope.show($stateParams.id)

  return