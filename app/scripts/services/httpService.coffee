true
'use strict'
debtTracker.service 'httpService', [
  '$http'
  '$state'
  ($http, $state) ->

    @get = (scope) ->
      $http.get(App.urlToServer + scope.entity.type).success((data) ->
        scope.entity[scope.entity.type] = data
        return
      ).error ->
        console.log 'Something went wrong in httpService.get for: ', scope.entity.type
        return
      return

    @getOne = (scope, id) ->
      $http.get(App.urlToServer + scope.entity.type + '/' + id).success((data) ->
        scope.entity[scope.entity.type] = data
        return
      ).error ->
        console.log 'Something went wrong in httpService.getOne for: ', scope.entity.type
        return
      return

    @post = (scope, payload) ->
      $http.post App.urlToServer + scope.entity.type, payload
        .success (data) ->
          scope.savedEntity = {}
          scope.savedEntity[scope.entity.type] = data
          $state.go 'home.' + scope.entity.type
          return
        .error (a, b, c, d) ->
          console.log 'Something went wrong in httpService.post for: ', scope.entity.type
          return
      return

    @del = (scope) ->
      $http.delete App.urlToServer + scope.entity.type + '/' + scope.entity[scope.entity.type].id
        .success (data) ->
          $state.go 'home.' + scope.entity.type, {}, {reload: true}
          return
        .error ->
          console.log 'Something went wrong in httpService.del deleting: ', scope.entity.type
          return
      return

    return this
]
