'use strict'

debtTracker
  .config ($stateProvider) ->
    $stateProvider
      .state 'home.debtee',
        url: 'debtee'
        controller: 'DebteeController'
        templateUrl: App.templateUrl + 'common/personIndex.html'
      .state 'home.debteeNew',
        url: 'debtee/new'
        controller: 'DebteeController'
        templateUrl: App.templateUrl + 'common/personNew.html'
      .state 'home.debteeShow',
        url: 'debtee/:id'
        controller: 'DebteeController'
        templateUrl: App.templateUrl + 'common/personShow.html'