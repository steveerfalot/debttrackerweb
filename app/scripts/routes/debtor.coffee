'use strict'

debtTracker
  .config ($stateProvider) ->
    $stateProvider
      .state 'home.debtor',
        url: 'debtor'
        controller: 'DebtorController'
        templateUrl: App.templateUrl + 'common/personIndex.html'
      .state 'home.debtorNew',
        url: 'debtor/new'
        controller: 'DebtorController'
        templateUrl: App.templateUrl + 'common/personNew.html'
      .state 'home.debtorShow',
        url: 'debtor/:id'
        controller: 'DebtorController'
        templateUrl: App.templateUrl + 'common/personShow.html'