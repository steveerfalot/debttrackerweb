'use strict'

debtTracker
  .config ($stateProvider, $urlRouterProvider) ->
    $urlRouterProvider.otherwise '/'

    $stateProvider
      .state 'home',
        url: '/'
        controller: 'MainController'
        templateUrl: App.templateUrl + 'main.html'
