'use strict'

debtTracker
.config ($stateProvider) ->
  $stateProvider
  .state 'home.debt',
    url: 'debt'
    controller: 'DebtController'
    templateUrl: App.templateUrl + 'debt/index.html'
  .state 'home.debtNew',
    url: 'debt/new'
    controller: 'DebtController'
    templateUrl: App.templateUrl + 'debt/new.html'
  .state 'home.debtShow',
    url: 'debt/:id'
    controller: 'DebtController'
    templateUrl: App.templateUrl + 'debt/show.html'