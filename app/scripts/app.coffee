'use strict'

window.App =
  urlToServer : 'http://localhost:8093/DebtTrackerWeb/'
  templateUrl : 'views/'



angular
.module('debtTracker', [
      'ngAnimate',
      'ngCookies',
      'ngResource',
      'ngRoute',
      'ngSanitize',
      'ngTouch'
      'ui.router'
    ])
window.debtTracker = angular.module 'debtTracker'
