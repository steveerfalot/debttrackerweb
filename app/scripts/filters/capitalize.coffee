true
'use strict'

debtTracker
.filter 'capitalize', () ->
  (input, scope) ->
    return input.toLowerCase().substring(0,1).toUpperCase()+input.substring(1);