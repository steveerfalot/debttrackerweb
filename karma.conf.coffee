# Karma configuration
# http://karma-runner.github.io/0.12/config/configuration-file.html
# Generated on 2015-02-28 using
# generator-karma 0.8.3

module.exports = (config) ->
  config.set

    basePath: ''

    frameworks: ['jasmine']

    files: [
      'bower_components/angular/angular.js'
      'bower_components/angular-mocks/angular-mocks.js'
      'bower_components/angular-animate/angular-animate.js'
      'bower_components/angular-cookies/angular-cookies.js'
      'bower_components/angular-resource/angular-resource.js'
      'bower_components/angular-route/angular-route.js'
      'bower_components/angular-sanitize/angular-sanitize.js'
      'bower_components/angular-touch/angular-touch.js'
      'bower_components/angular-ui-router/release/angular-ui-router.js'
      'app/scripts/*.coffee'
      'app/scripts/routes/*.coffee'
      'app/scripts/filters/*.coffee',
      'app/scripts/**/*.coffee'
      'app/views/**/*html'
      'test/unit/**/*.coffee'
    ]
    exclude: [
      '.tmp/**/*.*'
    ]

    port: 8081

    logLevel: config.LOG_INFO

    browsers: ['PhantomJS']

    autoWatch: true

    singleRun: false

    colors: true

    reporters: ['progress', 'coverage']

    preprocessors:
      'app/views/**/*.html': ['ng-html2js']
      'app/**/*.coffee': ['coffee']
      'app/**/*.coffee': ['coverage']
      'test/**/*.coffee': ['coffee']

    ngHtml2JsPreprocessor:
      stripPrefix: 'app/'
      moduleName: 'views'

    coverageReporter:
      type: 'html'
      dir: 'test/coverage/'

    proxies: '/': 'http://localhost:9000/'
#    URL root prevent conflicts with the site root
#    urlRoot: '_karma_'
